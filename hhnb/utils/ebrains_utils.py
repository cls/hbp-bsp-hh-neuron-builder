import os
import ebrains_drive
from ebrains_drive import BucketApiClient


def upload_to_bucket(bucket_name: str, file_path: str, token: str) -> str:
    """ Upload a file to the bucket """
    client = BucketApiClient(token=token)
    bucket = client.buckets.get_bucket(bucket_name)
    file_name = os.path.basename(file_path)
    bucket.upload(file_path, file_name)
    return file_name


def get_file_url(bucket_name: str, file_name: str, token: str) -> str:
    """ Get the url of a file in the bucket """
    client = BucketApiClient(token=token)
    bucket = client.buckets.get_bucket(bucket_name)
    file_handle = bucket.get_file(file_name)
    return file_handle.get_download_link()
