import os
import json


class ExecFileConf:
    """
    This class provide two useful static method to build the execution script that run the job on the HPC.
    """


    @staticmethod
    def write_nsg_exec(dst_dir, max_gen, offspring, job_name, mode='start'):
        """
        Write the execution script for the NSG system.

        Parameters
        ----------
        dst_dir : str
                  destination folder where the file will be written.
        max_gen : int
                  maximum number of generations used by opt_neuron.
        offspring : int
                    number of individuals in offspring used by opt_neuron.
        job_name : str
                   set job_name
        mode : str, optional, default='start'
               select the action mode to run the job on the HPC. Can be 'start' or 'resume'.
        """

        buffer = \
f"""
import os
import json

os.system('python3 opt_neuron.py --max_ngen={max_gen} --offspring_size={offspring} --{mode} --checkpoint ./checkpoints/checkpoint.pkl')
with open('resume_job_settings.json', 'w') as fd:
    json.dump({json.dumps({'offspring_size': offspring, 'max_gen': max_gen, 'job_name': job_name, 'hpc': 'nsg',})}, fd)
"""
        with open(os.path.join(dst_dir, 'init.py'), 'w') as fd:
            fd.write(buffer)

    @staticmethod
    def write_daint_exec(dst_dir, folder_name, offspring, max_gen, job_name, mode='start'):
        """
        Write the execution script for the Piz-Daint (UNICORE) system.

        Parameters
        ----------
        dst_dir : str
                  destination folder where the file will be written.
        folder_name : str
                      the model root folder name.
        max_gen : int
                  maximum number of generations used by opt_neuron.
        offspring : int
                    number of individuals in offspring used by opt_neuron.
        job_name : str
                   set job_name
        mode : str, optional, default='start'
               select the action mode to run the job on the HPC. Can be 'start' or 'resume'.
        """
        buffer_zipfolder = \
f"""
import os
import zipfile

retval = os.getcwd()
print("Current working directory %s" % retval)
os.chdir('..')
retval = os.getcwd()
print("Current working directory %s" % retval)

def zipdir(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))

zipf = zipfile.ZipFile('output.zip', 'w')
zipdir('{ folder_name }', zipf)
"""

        with open(os.path.join(dst_dir, 'zipfolder.py'), 'w') as fd:
            fd.write(buffer_zipfolder)

        SLURM_JOBID = '{SLURM_JOBID}'
        IPYTHON_PROFILE = '{IPYTHON_PROFILE}'
        CHECKPOINTS_DIR = '{CHECKPOINTS_DIR}'

        buffer_sbatch = \
f"""
#!/bin/bash -l

mkdir logs
#SBATCH --job-name=bluepyopt_ipyparallel
#SBATCH --error=logs/ipyparallel_%j.log
#SBATCH --output=logs/ipyparallel_%j.log
#SBATCH --partition=normal
#SBATCH --constraint=mc

export PMI_NO_FORK=1
export PMI_NO_PREINITIALIZE=1
export PMI_MMAP_SYNC_WAIT_TIME=300
#set -e
#set -x

module load daint-mc cray-python/3.9.4.1
# If needed for analysis

module use /apps/hbp/ich002/hbp-spack-deployments/softwares/23-02-2022/modules/tcl/cray-cnl7-haswell

# always load neuron module
module load neuron/8.0.2
module load py-matplotlib
module load py-bluepyopt

nrnivmodl mechanisms

export USEIPYP=1
export IPYTHONDIR="`pwd`/.ipython"
export IPYTHON_PROFILE=ipyparallel.${SLURM_JOBID}
ipcontroller --init --sqlitedb --ip='*' --profile=${IPYTHON_PROFILE} &
sleep 30
srun ipengine --profile=${IPYTHON_PROFILE} &
CHECKPOINTS_DIR="checkpoints"
BLUEPYOPT_SEED=1 python opt_neuron.py --offspring_size={offspring} --max_ngen={max_gen} --{mode} --checkpoint "${CHECKPOINTS_DIR}/checkpoint.pkl"
echo '{json.dumps({'offspring_size': offspring, 'max_gen': max_gen, 'job_name': job_name, 'hpc': 'cscs'})}' > resume_job_settings.json
python zipfolder.py
"""
        with open(os.path.join(dst_dir, 'ipyparallel.sbatch'), 'w') as fd:
            fd.write(buffer_sbatch)

    @staticmethod
    def write_galileo_exec(dst_dir, folder_name, offspring, max_gen, job_name, mode='start', **kwargs):
        """
        Write the execution script for the GALILEO (UNICORE) system.

        Parameters
        ----------
        dst_dir : str
                  destination folder where the file will be written.
        folder_name : str
                      the model root folder name.
        max_gen : int
                  maximum number of generations used by opt_neuron.
        offspring : int
                    number of individuals in offspring used by opt_neuron.
        job_name : str
                   set job_name
        mode : str, optional, default='start'
               select the action mode to run the job on the HPC. Can be 'start' or 'resume'.
        """

        if 'job_name' not in kwargs:
            kwargs['job_name'] = 'bluepyopt_neuron_job'
        if 'nodes' not in kwargs:
            kwargs['nodes'] = 1
        if 'cores' not in kwargs:
            kwargs['cores'] = 36
        if 'runtime' not in kwargs:
            # if runtime is not specified, set it to 30 minutes
            kwargs['runtime'] = '00:30:00'
        else:
            # TODO: convert the timestring format
            pass

        buffer_zip_folder = \
f"""
import os
import zipfile

retval = os.getcwd()
print("Current working directory %s" % retval)
os.chdir('..')
retval = os.getcwd()
print("Current working directory %s" % retval)

def zipdir(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))

zipf = zipfile.ZipFile('output.zip', 'w')
zipdir('{ folder_name }', zipf)
"""

        with open(os.path.join(dst_dir, 'zipfolder.py'), 'w') as fd:
            fd.write(buffer_zip_folder)

        SLURM_JOBID = '{SLURM_JOBID}'
        IPYTHON_PROFILE = '{IPYTHON_PROFILE}'
        CHECKPOINTS_DIR = '{CHECKPOINTS_DIR}'

        buffer_sbatch = \
f"""
#! /bin/bash -l

mkdir logs
#SBATCH -A icei_H_Ebrait
#SBATCH --job-name=bluepyopt_ipyparallel
#SBATCH --error=logs/ipyparallel_%j.log
#SBATCH --output=logs/ipyparallel_%j.log

export PMI_NO_FORK=1
export PMI_NO_PREINITIALIZE=1
export PMI_MMAP_SYNC_WAIT_TIME=300
#set -e
#set -x

module purge
module load gcc/10.2.0
module load intel/oneapi-2022--binary intelmpi/oneapi-2022--binary
module load python/3.8.12--intel--2021.4.0

module use /g100_work/icei_H_Ebrait/software-deployment/HBP/22-05-2023/modules/linux-centos8-cascadelake/

# Load bluepyopt
module load neuron/9.0.a6
module load py-bluepyopt/1.13.86 py-matplotlib/3.6.2

nrnivmodl mechanisms

export USEIPYP=1
export IPYTHONDIR="`pwd`/.ipython"
export IPYTHON_PROFILE=ipyparallel.${SLURM_JOBID}
ipcontroller --init --sqlitedb --ip='*' --profile=${IPYTHON_PROFILE} &
sleep 30
srun ipengine --profile=${IPYTHON_PROFILE} &
CHECKPOINTS_DIR="checkpoints"
BLUEPYOPT_SEED=1 python opt_neuron.py --offspring_size={offspring} --max_ngen={max_gen} --{mode} --checkpoint "${CHECKPOINTS_DIR}/checkpoint.pkl"
echo '{json.dumps({'offspring_size': offspring, 'max_gen': max_gen, 'job_name': job_name, 'hpc': 'cineca-g100'})}' > resume_job_settings.json
python zipfolder.py
"""
        with open(os.path.join(dst_dir, 'ipyparallel.sbatch'), 'w') as fd:
            fd.write(buffer_sbatch)

    @staticmethod
    def write_jusuf_exec(dst_dir, folder_name, offspring, max_gen, job_name, mode='start', **kwargs):
        """
        Write the execution script for the JUSUF-JSC (UNICORE) system.

        Parameters
        ----------
        dst_dir : str
                  destination folder where the file will be written.
        folder_name : str
                      the model root folder name.
        max_gen : int
                  maximum number of generations used by opt_neuron.
        offspring : int
                    number of individuals in offspring used by opt_neuron.
        job_name : str
                   set job_name
        mode : str, optional, default='start'
               select the action mode to run the job on the HPC. Can be 'start' or 'resume'.
        """

        if 'job_name' not in kwargs:
            kwargs['job_name'] = 'bluepyopt_ipyparallel'
        if 'nodes' not in kwargs:
            kwargs['nodes'] = 2
        if 'cores' not in kwargs:
            kwargs['cores'] = 1
        if 'runtime' not in kwargs:
            # if runtime is not specified, set it to the default value
            kwargs['runtime'] = '0-20:00:00'
        else:
            # TODO: convert the timestring format
            pass

        buffer_zip_folder = \
f"""
import os
import zipfile

retval = os.getcwd()
print("Current working directory %s" % retval)
os.chdir('..')
retval = os.getcwd()
print("Current working directory %s" % retval)

def zipdir(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))

zipf = zipfile.ZipFile('output.zip', 'w')
zipdir('{ folder_name }', zipf)
"""

        with open(os.path.join(dst_dir, 'zipfolder.py'), 'w') as fd:
            fd.write(buffer_zip_folder)

        SLURM_JOBID = '{SLURM_JOBID}'
        IPYTHON_PROFILE = '{IPYTHON_PROFILE}'
        CHECKPOINTS_DIR = '{CHECKPOINTS_DIR}'

        buffer_sbatch = \
f"""
#!/bin/bash -x
#SBATCH --error=logs/ipyparallel_%j.log
#SBATCH --output=logs/ipyparallel_%j.log
#SBATCH --partition=batch


set -e
set -x

module --force purge all

module load Stages/2024
module load Intel/2023.2.1 ParaStationMPI/5.9.2-1
module load Python/3.11.3
module load SciPy-Stack/2023a
module load SciPy-bundle/2023.07

module use /p/project/icei-hbp-2020-0013/software-deployment/HBP/jusuf/03-04-2024/modules/linux-rocky8-x86_64/Core/
module load py-bluepyopt

rm -rf x86_64
nrnivmodl mechanisms

export USEIPYP=1
export IPYTHONDIR="`pwd`/.ipython"
export IPYTHON_PROFILE=ipyparallel.${SLURM_JOBID}
# ipython profile create --profile=${IPYTHON_PROFILE}
ipcontroller --init --sqlitedb --ip='*' --profile=${IPYTHON_PROFILE} &
sleep 30
srun ipengine --profile=${IPYTHON_PROFILE} &

# CHECKPOINTS_DIR="checkpoints/run.${SLURM_JOBID}"
CHECKPOINTS_DIR="checkpoints"
# mkdir -p ${CHECKPOINTS_DIR}

BLUEPYOPT_SEED=1 python opt_neuron.py --offspring_size={offspring} --max_ngen={max_gen} --{mode} --checkpoint "${CHECKPOINTS_DIR}/checkpoint.pkl"
echo '{json.dumps({'offspring_size': offspring, 'max_gen': max_gen, 'job_name': job_name, 'hpc': 'jusuf-jsc'})}' > resume_job_settings.json
python zipfolder.py
"""
        with open(os.path.join(dst_dir, 'ipyparallel.sbatch'), 'w') as fd:
            fd.write(buffer_sbatch)
