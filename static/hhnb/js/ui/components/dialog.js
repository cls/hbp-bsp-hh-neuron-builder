class MessageDialog {

    static #createAlertDialog(level, title, msg) {
        console.log(level, title, msg);
        let alert = $("<div id='alert-dialog' role='alert'></>");
        let classes = "alert alert-dismissable fade ";
        let button;
        if (level != "warning") {
            button = "<button class='btn workflow-btn alert-dialog-button' data-bs-dismiss='alert' onclick='closeAlertDialog()'>Ok</button>";
        } else {
            button = "<button class='btn workflow-btn alert-dialog-button' data-bs-dismiss='alert' onclick='window.location.reload();'>Reload</button>";
        }
        switch (level) {
            case "danger":
                if (!title) {
                    title = "Error !";
                }
                classes += "alert-danger";
                break;

            case "warning":
                if (!title) {
                    title = "Unexpected behavior occurred !"
                }
                classes += "alert-warning";
                break;

            case "success":
                if (!title) {
                    title = "Success !";
                }
                classes += "alert-success";
                break;

            case "info":
            default:
                if (!title) {
                    title = "Info";
                }
                classes += "alert-info";
        }
        alert.addClass(classes);
        alert.append("\
            <h4 class='alert-heading' style='text-align: center;'>" + title + "</h4>\
            <br>\
            <p style='text-align: justify'>" + msg + "</p>\
            <hr>\
            <div class='row'>" + button + "</div>"
        );
        return alert;
    }

    static async #openDialog(level, text) {
        if (text.startsWith("{\"refresh_url\"")) {
            return false;
        }
        let alertDialog;
        let sm = splitTitleAndMessage(text);
        $("#shadow-layer").css("display", "block").addClass("show");
        if (sm) {
            alertDialog = this.#createAlertDialog(level, sm.title, sm.message);
        } else {
            alertDialog = this.#createAlertDialog(level, undefined, text);
        }
        $("body").append(alertDialog);
        alertDialog.addClass("show");
    }

    static openSuccessDialog(msg) {
        this.#openDialog("success", msg)
    }

    static openErrorDialog(msg) {
        this.#openDialog("danger", msg);
    }

    static openInfoDialog(msg) {
        this.#openDialog("info", msg);
    }

    static openReloadDialog(msg) {
        this.#openDialog("warning", msg);
    }
}


const updateNodeNumber = () => {
    switch($(".accordion-button.hpc.active").attr("name")) {
        case "DAINT-CSCS":
            $("#node-num").val(Math.floor($("#offspring").val() / 36) + 1);
            break;
        case "G100-CINECA":
            $("#node-num").val(Math.floor($("#offspring").val() / 48) + 1);
            break;
        // case "SA":
        //     if ($("#sa-hpc-dropdown-optset-btn").text().toLowerCase() == "pizdaint") {
        //         $("#node-num").val(Math.floor($("#offspring").val() / 36) + 1);
        //     }
        //     break;
    }
}

$("#offspring").on("change", () => {
    if ($(".accordion-button.hpc.active").attr("name") == "NSG" || $(".accordion-button.hpc.active").attr("name") == "SA") {
        return false;
    }
    let newVal = $("#offspring").val();
    $("#core-num").val(newVal);
    updateNodeNumber();
})


class UploadFileDialog {

    static async #open(label) {
        $("#overlaywrapper").css("display", "block");
        $("#overlayupload").css("display", "block");
        await sleep(10);
        $("#overlaywrapper").addClass("show");
        $("#overlayupload").addClass("show");
        $("#uploadFormLabel").html("<strong>" + label + "</strong>");
        $("#uploadFormButton").prop("disabled", true);
    }

    static async close() {
        console.debug("Close upload box")
        $("#overlayupload").removeClass("show");
        $("#overlaywrapper").removeClass("show");
        await sleep(500);
        $("#overlayupload").css("display", "none");
        $("#overlaywrapper").css("display", "none");
    }

    static openFeatures() {
        console.debug("Open features upload box");
        $("#formFile").prop("multiple", true).attr("accept", ".json");
        $("#uploadImg").css("display", "none");
        let label = "Upload features files (\"features.json\" and \"protocols.json\")";
        this.#open(label);
    }

    static openModel() {
        console.debug("Open model upload box");
        $("#formFile").prop("multiple", false).attr("accept", ".zip");
        $("#uploadImg").css("display", "none");
        let label = "Upload optimization settings (\".zip\")";
        this.#open(label);
    }

    static openAnalysisResult() {
        console.debug("Open analysis result upload box");
        $("#formFile").prop("multiple", false).attr("accept", ".zip");
        $("#uploadImg").css("display", "block");
        let label = "Upload model (\".zip\")"
        this.#open(label);
    }

}

// Enable apply button hpc selection
$(".accordion-button.hpc").on("click", async event => {
    $(".accordion-button.hpc").removeClass("active");
    $("#apply-param").prop("disabled", true);
    if (!$(event.currentTarget).hasClass("collapsed")) {
        $(event.currentTarget).addClass("active");
        $("#apply-param").prop("disabled", false);
    }
    updateNodeNumber();
    $(".accordion-button.hpc").addClass("no-input");
    if (event.currentTarget.id == "accordionNSG" || event.currentTarget.id == "accordionSA") {
        $("#node-num").prop("disabled", false);
        $("#core-num").prop("disabled", false);
    } else {
        $("#offspring").trigger("change");
        $("#node-num").prop("disabled", true);
        $("#core-num").prop("disabled", true);
    }

    await sleep(300);
    $(".accordion-button.hpc").removeClass("no-input");
});


$("#username_submit").on("input", () => {
    $("#username_submit").removeClass("is-valid is-invalid");
})
$("#password_submit").on("input", () => {
    $("#password_submit").removeClass("is-valid is-invalid");
})

$("#job-action-start").on("click", () => {
    $("#job-action").text("Start");

    let jobName = $("#job-name").val().toString();
    if (jobName.endsWith("_resume")) {
        $("#job-name").val($("#job-name").val().toString().split("_resume")[0]);
    }

    let jObj = JSON.parse(window.sessionStorage.getItem("job_settings"));
    OptimizationSettingsDialog.loadSettings(jObj, "start");
})

$("#job-action-resume").on("click", () => {
    $("#job-action").text("Resume");
    let jObj = JSON.parse(window.sessionStorage.getItem("job_settings"));
    OptimizationSettingsDialog.loadSettings(jObj, "resume");
})


class OptimizationSettingsDialog {

    static #resetSettingsDialog() {
        $(".accordion-button").removeClass("active").addClass("collapsed").attr("aria-expanded", "false");
        $(".accordion-collapse").removeClass("show");
    }

    static setDefaultValue() {
        console.debug("setting default values");

        $("#daint_project_id").val("");
        $("#galileo_project_id").val("").attr("placeholder", "Set project");

        $("#gen-max").val(2);
        $("#offspring").val(10).prop("disabled", false);
        $("#node-num").val(1);
        $("#core-num").val(10);
        $("#runtime").val(2);
    }

    static loadSettings(jObj, mode="start") {
        window.sessionStorage.setItem("job_settings", JSON.stringify(jObj));

        if (!jObj["service-account"]) {
            $("#accordionSA").addClass("disabled").prop("disabled", true);
            if (!$("#accordionSA").text().includes("unreachable")) {
                $("#accordionSA").append("&emsp;<b>*( temporarily unreachable )</b>");
            }
        }
        populateServiceAccountSettings(jObj["service-account"], "optset");

        let settings = jObj.settings;
        let resume = jObj.resume;

        $("#job-name").val($("#wf-title").text().split("Workflow ID: ")[1]);

        if (mode == "start" && !$.isEmptyObject(settings)) {
            $("#apply-param").prop("disabled", false)

            $("#gen-max").val(settings["gen-max"]);
            $("#offspring").val(settings["offspring"]);
            $("#node-num").val(settings["node-num"]);
            $("#core-num").val(settings["core-num"]);
            $("#runtime").val(settings["runtime"]);

            if (settings.hpc == "DAINT-CSCS") {
                $("#accordionDaint").addClass("active");
                $("#daintCollapse").addClass("show");
                $("#daint_project_id").val(settings.project);
            } else if (settings.hpc == "G100-CINECA") {
                $("#accordionGalileo").addClass("active");
                $("#galileoCollapse").addClass("show");
                $("#galileo_project_id").val(settings.project);
            } else if (settings.hpc == "JUSUF-JSC") {
                $("#accordionJusuf").addClass("active");
                $("#jusufCollapse").addClass("show");
                $("#jusuf_project_id").val(settings.project);
            } else if (settings.hpc == "NSG") {
                $("#accordionNSG").addClass("active");
                $("#nsgCollapse").addClass("show");
                if (settings["username_submit"]) {
                    $("#username_submit").addClass("is-valid").removeClass("is-invalid");
                } else {
                    $("#username_submit").addClass("is-invalid").removeClass("is-valid");
                }
                if (settings["password_submit"]) {
                    $("#password_submit").addClass("is-valid").removeClass("is-invalid");
                } else {
                    $("#password_submit").addClass("is-invalid").removeClass("is-valid");
                }
            } else if (settings.hpc == "SA") {
                $("#accordionSA").addClass("active");
                $("#saCollapse").addClass("show");
                if (Object.keys(settings).includes("sa-hpc")) {
                    $("#sa-hpc-dropdown-optset > button").html(settings["sa-hpc"].toUpperCase());
                    $("#sa-project-dropdown-optset > button").html(settings["sa-project"]).prop("disabled", false);
                    $(".dropdown-item.project." + settings["sa-hpc"]).removeClass("gone");
                }
                if ($("#sa-hpc-dropdown-optset-btn").text().toLowerCase() == "select hpc" ||
                    $("#sa-project-dropdown-optset-btn").text().toLowerCase() == "select project") {
                    $("#sa-project-dropdown-optset-btn").prop("disabled", true);
                    $("#apply-param").prop("disabled", true);
                }
            }
        } else if (mode == "resume") {
            if (!$.isEmptyObject(resume)) {
                $("#offspring").val(resume["offspring_size"]).prop("disabled", true);
                $("#gen-max").val("").attr("placeholder", "Last gen: " + resume["max_gen"].toString());
            }
            $("#job-name").val(resume.job_name ? resume.job_name + "_resume" : $("#job-name").val() + "_resume");
        }
    }

    static getJsonData() {
        const data = new Object();
        let hpc = $(".accordion-button.active").attr("name");

        data["mode"] = $("#job-action").text().toLowerCase();
        data["hpc"] = hpc;
        data["job_name"] = $("#job-name").val();
        data["gen-max"] = parseInt($("#gen-max").val());
        data["offspring"] = parseInt($("#offspring").val());
        data["node-num"] = parseInt($("#node-num").val());
        data["core-num"] = parseInt($("#core-num").val());
        data["runtime"] = $("#runtime").val();

        if (hpc == "DAINT-CSCS") {
            if ($("#daint_project_id").val() == "") {
                $("#daint_project_id").removeClass("is-valid").addClass("is-invalid");
                showWarningAlert('Please fill "Project ID" to apply settings and continue your workflow.');
                throw "daint_project empty";
            }
            data["project"] = $("#daint_project_id").val();
        } else if (hpc == "G100-CINECA") {
            if ($("#galileo_project_id").val() == "") {
                data["project"] = $("#galileo_project_id").attr("placeholder");
            } else {
                data["project"] = $("#galileo_project_id").val();
            }
        } else if (hpc == "JUSUF-JSC") {
            if ($("#jusuf_project_id").val() == "") {
                data["project"] = $("#jusuf_project_id").attr("placeholder");
            } else {
                data["project"] = $("#jusuf_project_id").val();
            }
        } else if (hpc == "NSG") {
            let nsgUser = $("#username_submit");
            let nsgPass = $("#password_submit");

            if (nsgUser.val() == "") {
                nsgUser.addClass("is-invalid");
            }
            if (nsgPass.val() == "") {
                nsgPass.addClass("is-invalid");
            }
            if (nsgUser.val() == "" || nsgPass.val() == "") {
                showWarningAlert("Please fill \"username\" and/or \"password\" to apply settings and continue your workflow.");
                throw "credentials empty";
            }

            if (nsgUser.val() == "" || nsgPass.val() == "") {
                if (nsgUser.val())
                nsgUser.addClass("is-invalid")

                if (!nsgUser.hasClass("is-valid") && !nsgPass.hasClass("is-valid")) {
                    showWarningAlert("Please fill \"username\" and/or \"password\" to apply settings and continue your workflow.");
                    throw "credentials empty";
                }
            } else {
                data["username_submit"] = nsgUser.val();
                data["password_submit"] = nsgPass.val();
            }
        }
        if (hpc == "SA") {
            data["sa-hpc"] = $("#sa-hpc-dropdown-optset > button").text().toLowerCase();
            data["sa-project"] = $("#sa-project-dropdown-optset > button").text().toLowerCase();
            if (data["sa-hpc"] === undefined || data["sa-project"] === undefined || data["sa-hpc"] === "select hpc" || data["sa-project"] === "select project") {
                showWarningAlert("Please select which HPC and/or Project to use behind the Service Account.");
                throw "Service Account HPC or Project not selected";
            }
        }

        for (let x in data) {
            if (typeof(data[x]) === 'number' && isNaN(data[x])) {
                showWarningAlert("Missing correct value for <b>" + x.toUpperCase() + "</b>");
                throw Error("Not a number error on " + x);
            }
        }

        return data;
    }

    static async open() {
        $("#overlaywrapper").css("display", "block");
        $("#overlayparam").css("display", "block");
        await sleep(10);
        $("#overlaywrapper").addClass("show")
        $("#overlayparam").addClass("show");
    }

    static async close() {
        $("#overlayparam").removeClass("show");
        $("#overlaywrapper").removeClass("show");
        await sleep(500);
        $("#overlayparam").css("display", "none");
        $("#overlaywrapper").css("display", "none");
        $("#apply-param").prop("disabled", true);
        this.#resetSettingsDialog();
    }
}


function replaceWithSelectElement(id, options) {
    $("#" + id).remove();
    $("#" + id + "InputGroup")
        .append("<select id='" + id + "' class='form-select' name='" + id + "'>");
    $.each(options, (index, value)  => {
        $("#" + id).append("<option>" + value + "</option>");
    });
}

class ModelRegistrationDialog {

    static async open() {
        showLoadingAnimation("Loading options...");
        let modelName = $("#wf-title").text().split("Workflow ID: ")[1];
        $.getJSON("/hh-neuron-builder/get-model-catalog-attribute-options")
            .then(async options => {
                console.debug(options);
                for (let key of Object.keys(options)) {
                    switch(key) {
                        case "species":
                            replaceWithSelectElement("modelSpecies", options[key]);
                            break;

                        case "brain_region":
                            replaceWithSelectElement("modelBrainRegion", options[key]);
                            break;

                        case "model_scope":
                            replaceWithSelectElement("modelScope", options[key]);
                            break;

                        case "abstraction_level":
                            replaceWithSelectElement("modelAbstraction", options[key]);
                            break;

                        case "cell_type":
                            replaceWithSelectElement("modelCellType", options[key]);
                            break;

                        case "license":

                            $("#modelLicense").empty();
                            $.each(options[key], (index, value) => {
                                $("#modelLicense").append("<option>" + value + "</option>");
                            })
                            break

                        default:
                    }
                }
                console.debug("modelName " + modelName);
                $("#modelName").val(modelName)

                $("#overlaywrapper").css("display", "block");
                $("#overlaywrapmodreg").css("display", "block");
                await sleep(10);
                $("#overlaywrapper").addClass("show");
                $("#overlaywrapmodreg").addClass("show");
                hideLoadingAnimation();
            }).catch(error => {
                hideLoadingAnimation();
                console.error("Error while getting options from model catalog: ", error);
                MessageDialog.openErrorDialog(error);
            }).always(() => {
                // hideLoadingAnimation();
            })
    }

    static async close() {
        $("#overlaywrapmodreg").removeClass("show");
        $("#overlaywrapper").removeClass("show");
        await sleep(500);
        $("#overlaywrapper").css("display", "none");
        $("#overlaywrapmodreg").css("display", "none");
    }

    static getFormData() {
        const formData = new FormData($("#modelRegisterForm")[0]);
        formData.append("modelPrivate", $("#modelPrivate").prop("checked"));
        for (let value of formData.values()) {
            console.debug(value);
        }
        for (let key of formData.keys()) {
            console.debug(key);
        }
        return formData;
    }
}


export { MessageDialog, UploadFileDialog, OptimizationSettingsDialog, ModelRegistrationDialog }